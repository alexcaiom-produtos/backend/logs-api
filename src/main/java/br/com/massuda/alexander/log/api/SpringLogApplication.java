package br.com.massuda.alexander.log.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
@Import({	
	br.com.massuda.alexander.spring.framework.rabbit.config.Configuracao.class,
	br.com.massuda.alexander.spring.framework.infra.config.Configuracao.class,
	br.com.massuda.alexander.spring.framework.infra.web.config.Configuracao.class
	})
@Configuration
public class SpringLogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringLogApplication.class, args);
	}

}
