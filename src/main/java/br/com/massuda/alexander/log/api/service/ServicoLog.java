/**
 * 
 */
package br.com.massuda.alexander.log.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.massuda.alexander.log.api.dao.LogDao;
import br.com.massuda.alexander.log.api.model.Log;

/**
 * @author Alex
 *
 */
@Service
public class ServicoLog {

	@Autowired
	private LogDao dao;
	
	public void salvar (Log log) {
		dao.insert(log);
	}
	
	public List<Log> pesquisar (String id) {
		return (List<Log>) dao.get(id);
	}
	
	public void excluir (String id) {
		dao.delete(id);
	}
	
}
