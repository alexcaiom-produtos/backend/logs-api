/**
 * 
 */
package br.com.massuda.alexander.log.api.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import br.com.massuda.alexander.log.api.model.Log;

/**
 * @author Alex
 *
 */
@Repository
public interface LogRepository extends MongoRepository<Log, String> {

}
