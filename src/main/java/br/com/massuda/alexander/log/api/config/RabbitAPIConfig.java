/**
 * 
 */
package br.com.massuda.alexander.log.api.config;

import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import br.com.massuda.alexander.log.api.listener.Receiver;

/**
 * @author Alex
 *
 */
@SpringBootConfiguration
public class RabbitAPIConfig {
	
	@Bean
	MessageListenerAdapter listenerAdapter(Receiver receiver) {
		return new MessageListenerAdapter(receiver, "receiveMessage");
	}

}
