/**
 * 
 */
package br.com.massuda.alexander.log.api.controller;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.massuda.alexander.log.api.model.Log;
import br.com.massuda.alexander.log.api.service.ServicoLog;

/**
 * @author Alex
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/log")
public class LogsController {

	@Autowired
	private ServicoLog servico;
	
	@ResponseBody
	@PostMapping
	public void incluir (@RequestBody Log log) {
		servico.salvar(log);
	}
	

	/**
	 * TODO - Implementar filtros de paginacao
	 * @param id
	 * @return
	 */
	@ResponseBody
	@GetMapping
	public List<Log> pesquisar (String id) {
		return servico.pesquisar(id);
	}
	
	@ResponseBody
	@DeleteMapping
	public void excluir (String... ids) {
		CompletableFuture.runAsync(() -> {
			Arrays.asList(ids).forEach(id -> {
				servico.excluir(id);
			});
			
		});
	}
	
}
