package br.com.massuda.alexander.log.api.dao;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import br.com.massuda.alexander.log.api.model.Log;

@Component
public class LogDao {

	@Autowired
    private LogRepository repository;

    public Log insert(Log log) {
        return repository.insert(log);
    }

    public Collection<Log> get(String id) {
    	if (!StringUtils.isEmpty(id)) {
    		Log log = repository.findById(id)
					.orElse(null);
			return Arrays.asList(log);
		}
        return repository.findAll();
    }

    public Optional<Log> getPersonInformationById(String id) {
        return repository.findById(id);
    }

    public Log updatePersonUsingId(String id, Log person) {
        Optional<Log> query = repository.findById(id);
        Log values = query.get();
        values.setId(person.getId());
        values.setMensagem(person.getMensagem());
        return repository.save(values);
    }

    public void delete(String id) {
        try {
            repository.deleteById(id);
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        }
    }

}
