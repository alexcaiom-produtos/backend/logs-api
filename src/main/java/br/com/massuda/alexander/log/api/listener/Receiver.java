package br.com.massuda.alexander.log.api.listener;

import java.util.concurrent.CountDownLatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.massuda.alexander.log.api.model.Log;
import br.com.massuda.alexander.log.api.service.ServicoLog;

@Component
public class Receiver {

	private CountDownLatch latch = new CountDownLatch(1);
	@Autowired
	private ServicoLog service;

	public void receiveMessage(org.springframework.messaging.Message<Object> message) {
		System.out.println("Received <" + message.getPayload() + ">");
		latch.countDown();
	}
	
	
	public void receiveMessage(org.springframework.amqp.core.Message message) {
		System.out.println("Received <" + new String(message.getBody()) + ">");
		latch.countDown();
	}
	
	public void receiveMessage(Object message) {
		System.out.println("Received <" + message.toString() + ">, class:" +message.getClass().getSimpleName());
		latch.countDown();
	}
	
	public void receiveMessage(Log log) {
		System.out.println("Received <" + log + ">");
		service.salvar(log);
		latch.countDown();
	}
	
	public void receiveMessage(byte[] message) {
		System.out.println("Received <" + new String(message) + ">, class:" +message.getClass().getSimpleName());
		receiveMessage(new Log(null, new String(message)));
		latch.countDown();
	}

	public CountDownLatch getLatch() {
		return latch;
	}

}
